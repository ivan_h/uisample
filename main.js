// express
var express = require('express');
var app = express();
// cross origin resource sharing
var cors = require('cors');
app.use(cors());
// router
var router = require('./modules/routes');
app.use('/', router);

// static files
app.use(express.static(__dirname + "/node_modules/angular"));
app.use(express.static(__dirname + "/node_modules/angular-animate"));
app.use(express.static(__dirname + "/node_modules/angular-loading-bar"));
app.use(express.static(__dirname + "/node_modules/angular-ui-router"));
app.use(express.static(__dirname + "/node_modules/bootstrap"));
app.use(express.static(__dirname + "/node_modules/jquery"));
app.use(express.static(__dirname + "/public"));

// set port
app.set("port", process.env.APP_PORT || 3000);
app.listen(3000, function(){
    console.info('App started on port: %d', app.get('port'));
});