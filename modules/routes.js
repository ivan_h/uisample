// router
var express = require('express');
var router = express.Router();
// mysql db
var mysql = require('mysql');
var config = require('./../modules/dbconfig');
var connPool = mysql.createPool(config);
// body parser
var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({extended: false}));
router.use(bodyParser.json());

//testdata
var data = require('./../modules/testdata');

router.get("/", function(req, res, next) {

    //runSqlQuery(connPool,
    //    '',
    //    []);

    res.send(data);
});

//Handle file not found - 400
router.use(function(req, res, next) {
    res.status(404);
    res.type("text/plain");
    res.send("File not found: " + req.originalUrl);
    console.error("File not found: %s", req.originalUrl);
});

//Error - 500
router.use(function(err, req, res, next) {
    console.error("Application error: %s", err);
});

module.exports = router;

function runSqlQuery(pool, req, res,query,param){
    //Get a connection from the pool
    pool.getConnection(function(err, conn) {
        checkErr(err,res);

        //Perform the query
        try {
            // in case you want to get data by parameter
            conn.query(query, // query
                param, //actual parameters based on position
                function (err, result) {
                    checkErr(err, res);
                    res.send(result);
                });
        } catch (ex) {
            res.send(err);
        } finally {
            conn.release();
        }
    });
}

function checkErr(err,res){
    if (err) {
        res.send(err);
        return;
}
    }
