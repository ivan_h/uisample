(function(){
    var app = angular.module('app',['ngAnimate', 'ui.router', 'ui.bootstrap', 'angular-loading-bar']);

    app.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeSpinner = true;
        cfpLoadingBarProvider.includeBar = true;
        cfpLoadingBarProvider.latencyThreshold = 50;
    }]);

    var ListCtrl = function($http, $state, FilmSvc) {
        var vm = this;
        vm.films = [];
        vm.displayDetails = function(film_id) {
            $state.go("details",
                { filmId: film_id });
        }

        var promise = FilmSvc.getAllFilms()
            .then(function(films) {
                vm.films = films

            }).catch(function(err) {
                console.error(">> %s", err)
            })

    };

    var DetailsCtrl = function($http, $state, $stateParams, FilmSvc, FilmFac) {
        var vm = this;
        vm.film = {};
        vm.back = function() {
            $state.go("list");
        }
        console.info(">> film = " + $stateParams.filmId)

        var result = FilmSvc.getFilm($stateParams.filmId);

        FilmModule.getFilm($stateParams.filmId)
            .then(function(film) {
                console.info(">>> returned " + result.data)
                vm.film = result.data;

            }).catch(function(err) {
                console.error(">> error = %s", err);
            })
    };

    var storeFac = function(){

    };

    var IMDBConfig = function($stateProvider, $urlRouterProvider) {

        //This is how a service is created
        //var FilmSvc = new __FilmSvc($http, $q);

        //This is how a factory is created
        //var FilmFac = __FilmFac($http, $q);

        $stateProvider.state("list", {
            url: "/list",
            templateUrl: "/views/list.html",
            controller: ["$http", "$state",
                "FilmSvc", "FilmFac", ListCtrl],
            controllerAs: "listCtrl"

        }).state("details", {
            url: "/film/:filmId",
            templateUrl: "/views/details.html",
            controller: ["$http", "$state", "$stateParams",
                "FilmSvc", "FilmFac", DetailsCtrl],
            controllerAs: "detailsCtrl"
        });

        $urlRouterProvider.otherwise("/list");
    }

    IMDBApp.config(["$stateProvider", "$urlRouterProvider", IMDBConfig]);






}());
